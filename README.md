# Investoo Code Test

We have provided mockup of an imagnary landing page (screeenshots provided for how it would look on desktop and mobile).

The task is to make a start on reimplementing these design in html and css (and potentially javascript) using laravel mix as your build process.

The boiler plate for this task is in place and will simply require you to run npm install from the root of the project.

We do not want you to spend any more than 30-60 mins on this project but want to make it clear that we have no expectation that in this time you were get anywhere near close to having a finished solution.

The point of the exercise is more to make a start to the project to then stimulate discussion in a face to face interview.

Designs are within /assets/img/

## Fonts

Google Font: Lato

## Build Process

https://laravel-mix.com/

### Install NPM packages

`npm install`

### Build Assets (JavaScript/CSS)

```
npm run dev
npm run prod
npm run watch
```